package enums;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum ProfitAggregation {

    MEAN_PROFIT(list -> {
        long total = 0;
        for (long l:list) {
            total += l;
        }
        return total/list.size();
    }),
    MEDIAN_PROFIT(list -> {
        List<Long> sortedList = list.stream().sorted().collect(Collectors.toList());
        if (sortedList.size()%2==0) {
            return (sortedList.get(sortedList.size()/2-1) + sortedList.get(sortedList.size()/2))/2;
        } else {
            return sortedList.get(sortedList.size()/2);
        }
    }),
    MEAN_OF_WORST_HALF_OF_DAYS(list -> {
        List<Long> sortedList = list.stream().sorted().collect(Collectors.toList());
        long total = 0;
        for (int i=0;i<sortedList.size()/2;i++) {
            total+=sortedList.get(i);
        }
        return total/(sortedList.size()/2);
    }),
    LOWEST(list -> {
        long min = list.get(0);
        for (int i=0;i<list.size();i++) {
            min = Math.min(min, list.get(i));
        }
        return min;
    });

    public final Function<ArrayList<Long>, Long> aggregate;

    ProfitAggregation(Function<ArrayList<Long>, Long> aggregator) {
        this.aggregate = aggregator;
    }

}
