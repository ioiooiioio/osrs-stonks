package util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Date addADay(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.DATE, 1);
        while (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            c.add(Calendar.DATE, 1);
        }
        d = c.getTime();
        return d;
    }

    public static Date nextWeekday(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        while (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            c.add(Calendar.DATE, 1);
        }
        d = c.getTime();
        return d;
    }

}
