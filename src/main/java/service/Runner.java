package service;

import api.DataMiner;
import api.Retry;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import dto.Mapping;
import dto.TimeseriesOutput;
import enums.ProfitAggregation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Runner {

    public static void main(String[] args) throws Exception {

        List<Mapping> mappings = new Gson().fromJson(new FileReader("data/mapping.json"), new TypeToken<List<Mapping>>(){}.getType());
        HashMap<Long, TimeseriesOutput> timeserieses = new HashMap<>();

        mappings.forEach(mapping -> timeserieses.put(mapping.getId(), new TimeseriesOutput()));

        long beginTimestamp = new Date().getTime() / 1000 / (60 * 60 * 24) * (60 * 60 * 24) - (60 * 60 * 24) * (10);
        long endTimestamp = beginTimestamp + 60 * 60 * 24 * 10;

        for (long t = beginTimestamp;t<=endTimestamp;t+=3600) {
            long timestamp = t;
            System.out.println(timestamp);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
            }
            JsonObject hourlyData = Retry.untilNotNull(() -> DataMiner.getHourlyTrades(timestamp)).get("data").getAsJsonObject();
            hourlyData.keySet().forEach(key -> {
                long itemId = Long.parseLong(key);
                JsonObject data = hourlyData.get(key).getAsJsonObject();
                long avgHighPrice = data.get("avgHighPrice").isJsonNull()?0:data.get("avgHighPrice").getAsLong();
                long avgLowPrice = data.get("avgLowPrice").isJsonNull()?0:data.get("avgLowPrice").getAsLong();
                long highPriceVolume = data.get("highPriceVolume").isJsonNull()?0:data.get("highPriceVolume").getAsLong();
                long lowPriceVolume = data.get("lowPriceVolume").isJsonNull()?0:data.get("lowPriceVolume").getAsLong();
                timeserieses.get(itemId).getData().add(new TimeseriesOutput.Data(avgHighPrice, avgLowPrice, highPriceVolume, lowPriceVolume, timestamp));
            });
        }

        FileWriter out = new FileWriter("output/trades.csv");
        out.write("ID\tBuy\tSell\tDaily Vol\tTotal Vol\tProfit\tName\n");
        mappings.forEach(mapping -> {
            TimeseriesOutput data = timeserieses.get(mapping.getId());
            TimeseriesOutput.TradingSummary tradingSummary = data.findBestTrades(4*mapping.getLimit(), ProfitAggregation.LOWEST);
            try {
                out.write(String.format("%d\t%d\t%d\t%d\t%d\t%d\t%s%n", mapping.getId(),
                        tradingSummary.getBuyPrice(), tradingSummary.getSellPrice(), tradingSummary.getDailyVolume(), tradingSummary.getTotalVolume(),
                        tradingSummary.getTotalVolume()*(tradingSummary.getSellPrice()-tradingSummary.getBuyPrice()),
                        mapping.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        out.flush();
        out.close();

//        TimeseriesOutput data = DataMiner.getTimeseries(12203);
//        System.out.println(data.findBestTrades(32, ProfitAggregation.MEAN_PROFIT));
//        System.out.println(data.findBestTrades(32, ProfitAggregation.MEDIAN_PROFIT));
//        System.out.println(data.findBestTrades(32, ProfitAggregation.MEAN_OF_WORST_HALF_OF_DAYS));
    }

}
