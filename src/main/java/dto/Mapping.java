package dto;

import lombok.Getter;

@Getter
public class Mapping {

    long id;
    int limit;
    String name;

}
