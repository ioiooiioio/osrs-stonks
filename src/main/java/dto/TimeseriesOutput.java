package dto;

import enums.ProfitAggregation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Data
public class TimeseriesOutput {

    List<Data> data = new ArrayList<>();

    @lombok.Data
    @AllArgsConstructor
    public static class Data {

        long avgHighPrice;
        long avgLowPrice;
        long highPriceVolume;
        long lowPriceVolume;
        long timestamp;

    }

    @AllArgsConstructor
    @Getter
    public static class TradingSummary {

        long buyPrice;
        long sellPrice;
        long dailyVolume;
        long totalVolume;

        public String toString() {
            return String.format("Buy Price:%d\t\tSell Price:%d\t\tVolume:%d", buyPrice, sellPrice, totalVolume);
        }

    }

    public TradingSummary findBestTrades(int volumeLimit, ProfitAggregation aggregator) {
        HashSet<Long> buys = new HashSet<>();
        HashSet<Long> sells = new HashSet<>();
        long beginTimestamp = new Date().getTime() / 1000 / (60 * 60 * 24) * (60 * 60 * 24) - (60 * 60 * 24) * 10;
        long endTimestamp = beginTimestamp + 60 * 60 * 24 * 10;

        for (int i=0;i<data.size();i++) {
            if (data.get(i).getTimestamp()<beginTimestamp || data.get(i).getTimestamp()>=endTimestamp) {
                data.remove(i);
                i--;
            } else {
                buys.add(data.get(i).getAvgLowPrice());
                sells.add(data.get(i).getAvgHighPrice());
            }
        }

        long bestAggregateScore = 0;
        long bestBuyPrice = 0, bestSellPrice = 0, bestAggVolume = 0, bestTotalVolume = 0;

        for(Long buyPrice:buys) {
            for (Long sellPrice:sells) {
                if (buyPrice<sellPrice) {
                    long[] buyVolumes = new long[10], sellVolumes = new long[10];
                    for (Data dataPoint:data) {
                        long index = (dataPoint.timestamp-beginTimestamp)/(60*60*24);
                        if (dataPoint.getAvgLowPrice()<=buyPrice) {
                            buyVolumes[(int)index] += dataPoint.getLowPriceVolume();
                        }
                        if (dataPoint.getAvgHighPrice()>=sellPrice) {
                            sellVolumes[(int)index] += dataPoint.getHighPriceVolume();
                        }
                    }
                    ArrayList<Long> twoWayVolume = new ArrayList<>();
                    for (int i=0;i<10;i++) {
                        twoWayVolume.add(Math.min(volumeLimit, Math.min(buyVolumes[i], sellVolumes[i])));
                    }
                    long aggVolume = aggregator.aggregate.apply(twoWayVolume);
                    long aggregateScore = aggVolume*(sellPrice-buyPrice);
                    if (aggregateScore > bestAggregateScore) {
                        bestAggregateScore = aggregateScore;
                        bestAggVolume = aggVolume;
                        bestBuyPrice = buyPrice;
                        bestSellPrice = sellPrice;
                        bestTotalVolume = 0;
                        for (Long l:twoWayVolume) {
                            bestTotalVolume += l;
                        }
                    }
                }
            }
        }

        return new TradingSummary(bestBuyPrice, bestSellPrice, bestAggVolume, bestTotalVolume);
    }

    public ArrayList<Long> computeProfitStats(int volumeLimit) {

        ArrayList<Long> costs = new ArrayList<>();
        ArrayList<Long> profits = new ArrayList<>();
        for (int batch = 0;batch<10;batch++) {
            long beginTimestamp = new Date().getTime() / 1000 / (60 * 60 * 24) * (60 * 60 * 24) - (60 * 60 * 24) * (batch + 1);
            long endTimestamp = beginTimestamp + 60 * 60 * 24;
            List<Long> buyPrices = new ArrayList<>();
            List<Long> sellPrices = new ArrayList<>();
            for (Data dataPoint : data) {
                if (dataPoint.getTimestamp() > beginTimestamp && dataPoint.getTimestamp() <= endTimestamp) {
                    for (int j = 0; j < dataPoint.getLowPriceVolume(); j++) {
                        buyPrices.add(dataPoint.getAvgLowPrice());
                    }
                    for (int j = 0; j < dataPoint.getHighPriceVolume(); j++) {
                        sellPrices.add(dataPoint.getAvgHighPrice());
                    }
                }
            }
            buyPrices = buyPrices.stream().sorted().collect(Collectors.toList());
            sellPrices = sellPrices.stream().sorted().collect(Collectors.toList());

            int index = 0;
            long profit = 0, cost = 0;
            while (index<=volumeLimit &&
                    index < buyPrices.size() && index < sellPrices.size() &&
                    buyPrices.get(index) < sellPrices.get(sellPrices.size() - 1 - index)) {
                cost += buyPrices.get(index);
                profit += sellPrices.get(sellPrices.size() - 1 - index) - buyPrices.get(index);
                index++;
            }
            costs.add(cost);
            profits.add(profit);
        }

        long totalCost = 0;
        long totalProfit = 0;
        for (Long c:costs) {
            totalCost += c;
        }
        for (Long p:profits) {
            totalProfit += p;
        }
        totalCost/=Math.max(1, costs.size());
        totalProfit/=Math.max(1, profits.size());

        ArrayList<Long> toReturn = new ArrayList<>();
        toReturn.add(totalCost);
        toReturn.add(totalProfit);

        return toReturn;

    }

}
