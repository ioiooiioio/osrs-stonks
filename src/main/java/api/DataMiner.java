package api;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dto.TimeseriesOutput;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DataMiner {

    public static TimeseriesOutput getTimeseries(long itemId) {
        String url = String.format("https://prices.runescape.wiki/api/v1/osrs/timeseries?timestep=1h&id=%d", itemId);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.header("User-Agent", "Daily GE Profit Calculator [Discord: ioiooiioio#3092]");

        Response response = request.get(url);
        if (response.getStatusCode()!=200) {
            System.out.println("FAILURE " + response.getStatusCode());
            return null;
        }
        return new Gson().fromJson(response.getBody().asString(), TimeseriesOutput.class);
    }

    public static JsonObject getHourlyTrades(long timestamp) {
        String url = String.format("https://prices.runescape.wiki/api/v1/osrs/1h?timestamp=%d", timestamp);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.header("User-Agent", "Daily GE Profit Calculator [Discord: ioiooiioio#3092]");

        Response response = request.get(url);
        if (response.getStatusCode()!=200) {
            System.out.println("FAILURE " + response.getStatusCode());
            return null;
        }
        return new Gson().fromJson(response.getBody().asString(), JsonObject.class);
    }

}
