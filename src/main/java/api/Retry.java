package api;

import java.util.function.Predicate;
import java.util.function.Supplier;

public class Retry {

    public static <T> T untilNotNull(Supplier<T> func) {
        T t = func.get();
        while (t==null) {
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
            }
            t = func.get();
        }
        return t;
    }

}
